# BlueBank Instrucciones para ejecutar el aplicativo

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://gitlab.com/anderson0496/bluebank)

Aplicativo prueba para Bluesoft.

  - Persistencia
  - BankEnd
  - FrontEnd
# Primeros Pasos!
Se debe instalar netCore para esto usaremos el siguiente enlace: https://dotnet.microsoft.com/download Descargaremos la version que sea la necesaria para nuestro sistema operativo.
Tambien necesitamos tener instalado Node.js esto lo descargaremos desde el siguiente enlace: https://nodejs.org/es/download/ de igal manera seleccionaremos la version disponible para el sistema operativo que tengamos.
El siguiente paso es instalar Angular cli: https://cli.angular.io/ para nuestro FrontEnd.
Para la persistencia se uso postgrest como gestor, en el siguiente enlace https://www.postgresql.org/download/ encontrar las instrucciones para su descarga he instalacion

# Persistencia!
Despues de tener instalado las herramientas necesarias, mediante nuestra consola ingresaremos a postgrest para crear nuestra base de datos, esto con los siguientes comandos conectados a la consola de postgrest.
```sh
$ psql
$ CREATE USER bluebank WITH PASSWORD 'bluebank';
$ CREATE DATABASE bluebank OWNER bluebank;	
```
Con esto ya existira la base de datos donde alojaremos nuestras tablas para almacenar datos.
Nuestro proyecto cuenta con CodeFirst, el cual por medio de las entidades creamos las tablas en nuestra base de datos, por lo tanto no nos debemos en preocupar por la creaciond e estas.

  
# BackEnd!
Para correr el aplicativo en su BackEnd se puede realizar de dos maneras.
  
  #### Desde visualStudio
  - Desde visualStudio el aplicativo se construye para descargar sus dependecias.
  - Luego en el Package Manager Console y seleccionando la capa Dal, ejecutamos el comando.
  -     Update-Database
  - Si las migraciones corren exitosamente podremos levantar ejecutando el servidor IIS express. esto nos abrira una pagina donde se cargara https://localhost:44398/swagger y podremos verificar los diferentes serivicios REST.

  #### Desde Consola
  - Desde consola nos tendremos que dirigir a la raiz del proyecto y ejecutar el comando.
  -     dotnet tool install --global dotnet-ef
  -     dotnet restore
  - Esto nos instalara las dependencias del proyecto.
  - luego ejecutaremos las migraciones con el comando
  -     dotnet ef database update
  - Si las migraciones corren exitosamente podremos levantar ejecutando el comando
  -     dotnet run    
  - Esto nos expondra los puerto 5000 y 5001 y en la url  https://localhost:5001/swagger/index.html y podremos verificar los diferentes serivicios REST.
# BackEnd!
para realizar la ejecucion de el FrontEnd, solo necesitamos desde consola dirigirnos a la raiz del proyecto FrontEnd/BlueBank y desde aqui ejecutar los siguientes pasos:
```sh
$ npm install
$ ng serve
```
Si todo fue exitoso podremos abrir en un navegador la ruta: http://localhost:4200/
Donde veremos el aplicativo con sus funciones ejecutandose.
# BlueBank Supuestos de negocio
Se requiere crear cuentas bancarias pero una persona puede tener multiples cuentas por lo tanto se dividio en dos entidades una Persona y otra Cuenta, con esto se desarrollo la base de datos que como se menciona anteriormente se usa Postgrest como motor de base de datos. se requiere que el aplicativo de escalable por lo tanto se piensa en dividir el trabajo en multiples capas. y proyectos separados BackEnd y FrontEnd.
# BlueBank Arquitectura implementada
Para este proyecto se usa una arquitectura multicapa en el BackEnd con orientacion a dominio donde los modelos cuentan con logica para realizar acciones propias de ellos como lo es la generacion de el numero de cuenta o las transacciones ejecutadas. Cada capa cuenta con su propio modelos de manipulacion como los es, DTOs, Models y Entidades. Esto para no contar con ropturas de fronteras y tener un codigo mas limpio y escalable.
En la persistencia se usa CodeFirst con esto podemos manipular la estructura de la base de datos desde las entidades de nuestro codigo.
Para el FrontEnd se realiza la creacion de componentes destinados para acciones es decir cada componente se encarga de una accion en particular.

# BlueBank Tecnologias seleccionadas
En cuanto a el BackEnd se usa NetCore 3.1 que a la fecha es uno de los framework mas poderosos del mundo con lenguaje C#. tambien contando con la linea de la tecnologia Microsoft se usa Entity framework Core para la manipulacion y manejo de los datos a nivel de persistencia. Se usa swagger para la documentacion de servicios y de misma forma prueba de ellos. Cuenta con Log4Net para el control por medio de archivos de fallos o ciertos mensajes que se deseen guardar en log. Postgrest como motor de base de datos ya que es OpenSource y esta al nivel de tecnologias de punta. para la pruebas se implemento UnitTest para la realizacion de estas. En el FrontEnd se usa Angular 9 con su estructura de componentes y Bootstrap como framework de estilos. tambien se implemento Jspdf para imprimir los soportes.

# BlueBank ¿Que se puesde mejorar?
Con mas tiempo se puede implementar JWT para que los servicios esten protegidos, tambien definir roles ya que no todos pueden crear cuentas y al mismo tiempo realizar transacciones, tambien implementar un control de fallos el cual se escalable y a posteriori tener una arquitectura horizontal.
Implementar un login para el control de los diferentes roles existentes y a las vistas que puedan acceder.

