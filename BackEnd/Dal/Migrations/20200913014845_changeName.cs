﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Dal.Migrations
{
    public partial class changeName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "Account");

            migrationBuilder.AddColumn<float>(
                name: "Balance",
                table: "Account",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Balance",
                table: "Account");

            migrationBuilder.AddColumn<float>(
                name: "Amount",
                table: "Account",
                type: "real",
                nullable: false,
                defaultValue: 0f);
        }
    }
}
