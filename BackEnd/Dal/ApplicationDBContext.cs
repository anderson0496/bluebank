﻿using Entities.BlueBank;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dal
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options) { }

        public DbSet<PersonEntity> Person { get; set; }
        public DbSet<AccountEntity> Account { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<AccountEntity>()
                .HasIndex(account => account.NumberAccount)
                .IsUnique(true);

        }
    }
}
