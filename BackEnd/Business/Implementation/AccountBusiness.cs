﻿using AutoMapper;
using Business.Repository;
using Common.Message;
using Core.Interfaces;
using Core.Models;
using Entities.BlueBank;
using Microsoft.Extensions.Logging;
using System;

namespace Business.Implementation
{
    public class AccountBusiness : IAccount
    {
        private readonly IRepositoryBase<PersonEntity> repository;
        private readonly IRepositoryBase<AccountEntity> repositoryAccount;
        private readonly ILogger<AccountBusiness> logger;
        private readonly IMapper mapper;

        public AccountBusiness(IRepositoryBase<PersonEntity> _repository, IRepositoryBase<AccountEntity> _repositoryAccount, IMapper _mapper, ILogger<AccountBusiness> _logger)
        {
            this.repository = _repository;
            repositoryAccount = _repositoryAccount;
            this.mapper = _mapper;
            this.logger = _logger;
        }

        public Person CreateAccount(Person person)
        {
            try
            {
                person.Account.GenerateNumberAccount();
                var personCreated = this.repository.Create(mapper.Map<PersonEntity>(person));
                return mapper.Map<Person>(personCreated);
            }
            catch (Exception ex)
            {
                this.logger.LogInformation(string.Format(MessageConstant.FailCreationBankAccount, person.Name));
                throw new Exception(ex.Message);
            }   
        }

        public bool MakeDeposit(Transaction transaction)
        {
            bool status = false;
            try
            {
                var account = this.SearchByNumberAccount(transaction.NumberAccount);
                if (account.MakeDeposit(transaction.Value))
                {
                    this.repositoryAccount.Update(mapper.Map<AccountEntity>(account));
                    status = true;
                }
                return status;
            }
            catch (Exception ex)
            {
                this.logger.LogInformation(string.Format(MessageConstant.FailMakeDepositBankAccount, transaction.NumberAccount));
                throw new Exception(ex.Message);
            }
            
        }

        public bool MakeWithdrawal(Transaction transaction)
        {
            bool status = false;
            try
            {
                var account = this.SearchByNumberAccount(transaction.NumberAccount);
                if (account.MakeWithdrawal(transaction.Value))
                {
                    this.repositoryAccount.Update(mapper.Map<AccountEntity>(account));
                    status = true;
                }
                return status;
            }
            catch (Exception ex)
            {
                this.logger.LogInformation(string.Format(MessageConstant.FailMakeWithdrawalBankAccount, transaction.NumberAccount));
                throw new Exception(ex.Message);
            }
        }

        public Account SearchByNumberAccount(string numberAccount)
        {
            try
            {
                var response = this.repositoryAccount.GetByExpression(data => data.NumberAccount == numberAccount);
                return mapper.Map<Account>(response);

            } catch(Exception ex)
            {
                this.logger.LogInformation(string.Format(MessageConstant.FailSearchBankAccount, numberAccount));
                throw new Exception(ex.Message);
            }
        }
    }

}
