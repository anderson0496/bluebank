﻿using Core.Interfaces;
using Dal;
using Entities.BlueBank;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Business.Repository
{
    public class RepositorioBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        private readonly ApplicationDBContext context;
        public RepositorioBase(ApplicationDBContext _context)
        {
            this.context = _context;
        }
        public TEntity Create(TEntity record)
        {
            this.context.Set<TEntity>().Add(record);
            this.context.SaveChanges();
            this.context.Entry(record).State = EntityState.Detached;
            return record;
        }

        public void Delete(TEntity record)
        {
            this.context.Set<TEntity>().Remove(record);
            this.context.SaveChanges();
        }

        public void Dispose()
        {
            this.context.Dispose();
        }

        public List<TEntity> GetAll()
        {
            return this.context.Set<TEntity>().ToList();
        }


        public TEntity GetByExpression(Expression<Func<TEntity, bool>> expr)
        {
            return this.context.Set<TEntity>().AsNoTracking().FirstOrDefault(expr);
           
        }

        public void Update(TEntity record)
        {
            this.context.Set<TEntity>().Update(record);
            this.context.SaveChanges();
            this.context.Entry(record).State = EntityState.Detached;
        }
    }
}
