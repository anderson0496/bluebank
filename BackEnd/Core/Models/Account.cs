﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class Account
    {
        public int Id { get; set; }
        public string NumberAccount { get; set; }
        public float Balance { get; set; }

        public Account(){ }

        public void GenerateNumberAccount()
        {
            this.NumberAccount = new Random(Guid.NewGuid().GetHashCode()).Next(111111111, 999999999).ToString("D10");
        }

        public Boolean MakeDeposit(float value)
        {
            Boolean status = false;
            if(value >= 0)
            {
                this.Balance += value;
                status = true;
            } 
            return status;
        }

        public Boolean MakeWithdrawal(float value)
        {
            Boolean status = false;
            if (value < this.Balance && value >= 0)
            {
                this.Balance -= value;
                status = true;
            }
            return status;
        }

        
    }
}
