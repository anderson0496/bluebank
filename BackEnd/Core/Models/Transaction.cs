﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class Transaction
    {
        public Transaction()
        {
        }

        public string NumberAccount { get; set; }
        public float Value { get; set; }
    }
}
