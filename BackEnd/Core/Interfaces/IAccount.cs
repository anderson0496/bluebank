﻿using Core.Models;

namespace Core.Interfaces
{
    public interface IAccount
    {
        public Person CreateAccount(Person person);

        public Account SearchByNumberAccount(string numberAccount);

        public bool MakeDeposit(Transaction transaction);
        
        public bool MakeWithdrawal(Transaction transaction);

    }
}
