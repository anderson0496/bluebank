﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Core.Interfaces
{
    public interface IRepositoryBase<TEntity> : IDisposable where TEntity : class
    {
        /// <summary>
        /// Get All Register
        /// </summary>
        /// <returns>List Register</returns>
        public List<TEntity> GetAll();

        /// <summary>
        /// Create Register
        /// </summary>
        /// <param name="record">Object Create</param>
        /// <returns>Primary Key</returns>
        public TEntity Create(TEntity record);

        /// <summary>
        /// Update Register
        /// </summary>
        /// <param name="record">Object Update</param>
        public void Update(TEntity record);

        /// <summary>
        /// Update Status Register
        /// </summary>
        /// <param name="id">Primary key</param>
        public void Delete(TEntity record);

        /// <summary>
        /// Get Object by Expression
        /// </summary>
        /// <param name="expr">Expression</param>
        /// <returns>Object TEntity</returns>
        public TEntity GetByExpression(Expression<Func<TEntity, bool>> expr);
    }
}
