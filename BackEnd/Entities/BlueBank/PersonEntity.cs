﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.BlueBank
{
    public class PersonEntity
    {
        public PersonEntity(){}
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual AccountEntity Account { get; set; }
    }
}
