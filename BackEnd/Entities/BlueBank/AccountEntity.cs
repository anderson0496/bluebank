﻿using System.ComponentModel.DataAnnotations;

namespace Entities.BlueBank
{
    public class AccountEntity
    {
        public AccountEntity(){}
        [Key]
        public int Id { get; set; }
        [Required]
        public string NumberAccount { get; set; }
        public float Balance { get; set; }
    }
}
