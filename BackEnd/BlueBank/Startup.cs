using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dal;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Core.Interfaces;
using Business.Implementation;
using AutoMapper;
using Entities.BlueBank;
using Business.Repository;

namespace BlueBank
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddCors(
                options => options.AddPolicy("AllowCors",
                builder =>
                {
                    builder
                    .WithOrigins("http://localhost:4200")
                    .AllowAnyOrigin()
                    .WithMethods("GET", "PUT",
                    "POST", "DELETE")
                    .AllowAnyHeader();
                })
            );

            // Add config AutoMapper
            services.AddAutoMapper(typeof(Startup));

            

            // Business services
            services.AddScoped<IAccount, AccountBusiness>();

            // Add Repository
            services.AddScoped<IRepositoryBase<PersonEntity>, RepositorioBase<PersonEntity>>();
            services.AddScoped<IRepositoryBase<AccountEntity>, RepositorioBase<AccountEntity>>();

            // Swagger
            object p = services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Swagger BlueBank",
                    Version = "v1",
                    Description = "REST Api service for Integration with BlueBank",
                    Contact = new OpenApiContact()
                    {
                        Name = "Anderson Arevalo"
                    }
                });
            });


            // Database Connection
            services.AddDbContext<ApplicationDBContext>(optionsAction: opt => opt.UseNpgsql(Configuration.GetConnectionString("Connection"), options => options.SetPostgresVersion(new Version(9, 6))));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            loggerFactory.AddLog4Net();
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors("AllowCors");
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Swagger BlueBank V1");
            });

        }
    }
}
