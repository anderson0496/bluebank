﻿using AutoMapper;
using BlueBank.DTOs;
using Core.Interfaces;
using Core.Models;
using Microsoft.AspNetCore.Mvc;

namespace BlueBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccount account;
        private readonly IMapper mapper;
        private IActionResult result;
        public AccountController(IAccount _account, IMapper _mapper)
        {
            this.account = _account;
            this.mapper = _mapper;
        }
        [HttpPost]
        public IActionResult ManualPayment([FromBody] PersonCreationDto personDto)
        {
            return this.validateRequest(this.mapper.Map<PersonDto>(this.account.CreateAccount(this.mapper.Map<Person>(personDto))));
        }

        [HttpPut("MakeDeposit")]
        public IActionResult MakeDeposit([FromBody] TransactionDto transaction)
        {
            if (this.account.MakeDeposit(this.mapper.Map<Transaction>(transaction)))
            {
                result = Ok();
            }
            else
            {
                result = BadRequest();
            }
            return result;
        }

        [HttpPut("MakeWithdrawal")]
        public IActionResult MakeWithdrawal([FromBody] TransactionDto transaction)
        {
            if (this.account.MakeWithdrawal(this.mapper.Map<Transaction>(transaction)))
            {
                result = Ok();
            }
            else
            {
                result = BadRequest();
            }
            return result;
        }

        [HttpGet("{numberAccount}")]
        public IActionResult SearchNyNumberAccount(string numberAccount)
        {
            return this.validateRequest(this.mapper.Map<AccountDto>(this.account.SearchByNumberAccount(numberAccount)));
        }

        private IActionResult validateRequest(object BankAccount)
        {
            if (BankAccount != null)
            {
                result =  Ok(BankAccount);
            }
            else
            {
                result = NotFound();
            }
            return result;
        }

    }
}
