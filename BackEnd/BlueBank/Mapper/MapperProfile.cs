﻿using AutoMapper;
using BlueBank.DTOs;
using Core.Models;
using Entities.BlueBank;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlueBank.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Person, PersonCreationDto>().ReverseMap();
            CreateMap<Account, AccountCreationDto>().ReverseMap();
            CreateMap<Person, PersonDto>().ReverseMap();
            CreateMap<Account, AccountDto>().ReverseMap();
            CreateMap<PersonEntity, Person>().ReverseMap();
            CreateMap<AccountEntity, Account>().ReverseMap();
            CreateMap<Transaction, TransactionDto>().ReverseMap();
        }
    }
}
