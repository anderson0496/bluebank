﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlueBank.DTOs
{
    public class PersonCreationDto
    {
        public string Name { get; set; }
        public AccountCreationDto Account { get; set; }
    }
}
