﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlueBank.DTOs
{
    public class TransactionDto
    {
        [Required]
        public string NumberAccount { get; set; }
        [Required]
        [Range(1, Int32.MaxValue, ErrorMessage = "El valor no puede ser negativo")]
        public float Value { get; set; }

    }
}
