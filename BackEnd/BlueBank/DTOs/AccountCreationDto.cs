﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlueBank.DTOs
{
    public class AccountCreationDto
    {
        [Required]
        [Range(1, Int32.MaxValue, ErrorMessage = "El valor no puede ser negativo")]
        public float Balance { get; set; }
    }
}
