﻿using Core.Interfaces;
using Core.Models;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;


namespace Test.Business
{
    public class AccountBusinessTest
    {
        private Person person;
        private Account account;
        private Transaction transaction;
        private IAccount accountService;
        [SetUp]
        public void Setup()
        {
            person = Substitute.For<Person>();
            transaction = Substitute.For<Transaction>();
            account = Substitute.For<Account>();
            accountService = Substitute.For<IAccount>();
        }
        [Test]
        public void WhenCreate_shouldReturnObject()
        {
            //Given
            person.Account = account;

            //When
            var response = accountService.CreateAccount(person).Returns(person);

            //Then
            Assert.IsNotNull(response);

        }

        [Test]
        public void WhenMakeDepositIsCorrect_shouldReturnValidation()
        {
            //Given
            person.Account = account;
            

            //When
            var response = accountService.MakeDeposit(transaction).Returns(true);

            //Then
            Assert.IsNotNull(response);

        }

        public void WhenConsultingBalance_shouldReturnObject()
        {
            //Given
            person.Account = account;


            //When
            var response = accountService.SearchByNumberAccount("123456").Returns(account);

            //Then
            Assert.IsNotNull(response);

        }

        [Test]
        public void WhenMakeWithdrawalIsCorrect_shouldReturnValidation()
        {
            //Given
            person.Account = account;


            //When
            var response = accountService.MakeDeposit(transaction).Returns(true);

            //Then
            Assert.IsNotNull(response);

        }
    }
}
