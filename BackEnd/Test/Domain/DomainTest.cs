﻿using Core.Interfaces;
using Core.Models;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Domain
{
    class DomainTest
    {
        private Account account;
        [SetUp]
        public void Setup()
        {
            account = Substitute.For<Account>();
          
        }

        [Test]
        public void WhenGenerateNumberAccount_shouldReturnObject()
        {
            //Given
            account.GenerateNumberAccount();

            //When
            var response = account.NumberAccount;

            //Then
            Assert.IsNotNull(response);

        }

        [Test]
        public void WhenMakeDeposit_shouldReturnTrue()
        {
            //Given
            account.Balance = 2000;

            //When
            var response = account.MakeDeposit(1000);

            //Then
            Assert.IsTrue(response);

        }
        [Test]
        public void WhenMakeDepositIsNotCorrect_shouldReturnTrue()
        {
            //Given
            account.Balance = 2000;

            //When
            var response = account.MakeDeposit(-1000);

            //Then
            Assert.IsFalse(response);

        }

        [Test]
        public void WhenMakeWithdrawal_shouldReturnTrue()
        {
            //Given
            account.Balance = 2000;

            //When
            var response = account.MakeWithdrawal(1000);

            //Then
            Assert.IsTrue(response);

        }
        [Test]
        public void WhenMakeWithdrawalIsNotCorrect_shouldReturnTrue()
        {
            //Given
            account.Balance = 2000;

            //When
            var response = account.MakeWithdrawal(3000);

            //Then
            Assert.IsFalse(response);

        }
    }
}
