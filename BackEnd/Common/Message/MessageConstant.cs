﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Message
{
    public static class MessageConstant
    {
        public const string FailCreationBankAccount = "Ocurrio un fallo al crear la cuenta con nombre de propietario. {0}";
        public const string FailSearchBankAccount = "No se encuentra una cuenta bancaria con número. {0}";
        public const string FailMakeDepositBankAccount = "No se pudo realizar el deposito correctamente al numero de cuenta. {0}";
        public const string FailMakeWithdrawalBankAccount = "No se pudo realizar el retiro correctamente al numero de cuenta. {0}";
        
    }
}
