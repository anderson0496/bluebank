import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import {CreateAccountService} from '../services/create-account.service';
import { CreateAccount } from '../models/create-account';
import {AccountS} from '../models/account-s';
import { jsPDF } from 'jspdf';
import {PersonFull} from '../models/person-full';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {
  form: FormGroup;
  accountCreation: CreateAccount;
  account: AccountS;
  constructor(private accountServie: CreateAccountService) {
    this.form = this.createForm();
  }

  ngOnInit() {

  }

  submit() {
    if (this.form.valid) {
      this.CreateAccount();
    } else {
      Swal.fire({
        title: 'Campos Obligatorios',
        text: 'Diligencie los campos porfavor',
        icon: 'warning',
        confirmButtonText: 'Cerrar'
      });
    }
  }
  CreateAccount() {
    this.account = {balance: this.value.value};
    this.accountCreation = new CreateAccount(this.account, this.fullName.value);
    this.accountServie.createBankAccount(this.accountCreation).subscribe(response => {
      Swal.fire({
        title: 'Exito',
        text: 'Se ha creado la cuenta exitosamente',
        icon: 'success',
        confirmButtonText: 'Cerrar'
      });
      this.GeneratePdf(response);
    }, error => {
      const errors = error.error.errors;
    });
  }
  get fullName() { return this.form.get('fullName'); }
  get value() { return this.form.get('value'); }
  createForm() {
    return new FormGroup({
      fullName: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required)
    });
  }
  GeneratePdf(data: PersonFull) {
    const date = new Date();
    const doc = new jsPDF();
    doc.setFontSize(20);
    doc.text('Gracias por crear su cuenta con Blue Bank' , 50, 10);
    doc.setFontSize(12);
    doc.text('Fecha de Consulta: ' + date.toString() , 10, 20);
    doc.text('Nombre de Titular: ' + data.name, 10, 30 );
    doc.text('Su número de cuenta es: ' + data.account.numberAccount, 10, 40);
    doc.save( `${data.name}-${data.account.numberAccount}BlueBank.pdf`);
  }

}
