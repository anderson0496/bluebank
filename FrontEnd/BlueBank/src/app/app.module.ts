import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgbNavModule} from '@ng-bootstrap/ng-bootstrap';
import { CreateAccountComponent } from './create-account/create-account.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ConsultingBalanceComponent } from './consulting-balance/consulting-balance.component';
import { MakeDepositComponent } from './make-deposit/make-deposit.component';
import { MakeWithdrawalComponent } from './make-withdrawal/make-withdrawal.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateAccountComponent,
    ConsultingBalanceComponent,
    MakeDepositComponent,
    MakeWithdrawalComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbNavModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
