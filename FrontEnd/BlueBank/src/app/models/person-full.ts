import {AcountFull} from './acount-full';

export class PersonFull {
  id: number;
  name: string;
  account: AcountFull;
  constructor(id: number, name: string, account: AcountFull) {
    this.id = id;
    this.name = name;
    this.account = account;
  }
}
