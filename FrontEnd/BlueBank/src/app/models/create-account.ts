import {AccountS} from './account-s';

export class CreateAccount {
  name: string;
  account: AccountS;

  constructor(account: AccountS, name: string) {
    this.name = name;
    this.account = account;
  }


}
