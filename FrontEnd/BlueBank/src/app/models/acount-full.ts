export class AcountFull {
  id: number;
  numberAccount: string;
  balance: number;
  constructor(id: number, numberAccount: string, balance: number) {
    this.id = id;
    this.numberAccount = numberAccount;
    this.balance = balance;
  }
}
