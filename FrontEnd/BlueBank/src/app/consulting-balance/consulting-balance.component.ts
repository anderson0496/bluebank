import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import {AccountS} from '../models/account-s';
import {AcountFull} from '../models/acount-full';
import {CreateAccountService} from '../services/create-account.service';
import {PersonFull} from '../models/person-full';
import {jsPDF} from "jspdf";
import {formatCurrency} from '@angular/common';

@Component({
  selector: 'app-consulting-balance',
  templateUrl: './consulting-balance.component.html',
  styleUrls: ['./consulting-balance.component.css']
})
export class ConsultingBalanceComponent implements OnInit {
  form: FormGroup;
  account: AcountFull = new AcountFull(0, '', 0);
  constructor(private accountServie: CreateAccountService) {
    this.form = this.createForm();
  }

  ngOnInit() {
  }

  submit() {
    if (this.form.valid) {
      this.consultingBalnace();
    } else {
      Swal.fire({
        title: 'Campos Obligatorios',
        text: 'Diligencie los campos porfavor',
        icon: 'warning',
        confirmButtonText: 'Cerrar'
      });
    }
  }
  consultingBalnace() {
    const numberAccount = this.numberAccount.value.trim();
    this.accountServie.consultingBalance(numberAccount).subscribe(response => {
      this.account = {id: response.id, numberAccount: response.numberAccount, balance: response.balance };
      Swal.fire({
        title: 'Exito',
        text: 'Este es su saldo actual',
        icon: 'success',
        timer: 3000,
        confirmButtonText: 'Cerrar'
      });
    }, error => {
      this.form.reset();
      this.account = new AcountFull(0, '', 0);
      Swal.fire({
        title: 'No se encontraron resultado',
        text: 'No se encuentra un resultado con el numero de cuenta ' + numberAccount,
        icon: 'info',
        confirmButtonText: 'Cerrar'
      });
    });
  }


  get numberAccount() { return this.form.get('NumberAccount'); }

  createForm() {
    return new FormGroup({
      NumberAccount: new FormControl('', Validators.required)
    });
  }

  GeneratePdf() {
    const date = new Date();
    const doc = new jsPDF();
    doc.setFontSize(20);
    doc.text('Saldos con Blue Bank' , 60, 10);
    doc.setFontSize(12);
    doc.text('Fecha de Consulta: ' + date.toString() , 10, 20);
    doc.text('Su número de cuenta es: ' + this.numberAccount.value, 10, 30);
    doc.text('Su saldo de cuenta es: $' + formatCurrency(this.account.balance, 'en', ''), 10, 40);
    doc.save( `${this.numberAccount.value}-BlueBank.pdf`);
  }

}
