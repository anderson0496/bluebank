import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultingBalanceComponent } from './consulting-balance.component';

describe('ConsultingBalanceComponent', () => {
  let component: ConsultingBalanceComponent;
  let fixture: ComponentFixture<ConsultingBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultingBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultingBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
