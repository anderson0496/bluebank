import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
// Import Enviroment
import { environment} from '../../environments/environment';
import {CreateAccount} from '../models/create-account';
import {PersonFull} from '../models/person-full';
import {AcountFull} from '../models/acount-full';
import {filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CreateAccountService {

  constructor(private http: HttpClient) { }

  createBankAccount(createAccount: CreateAccount) {
    const urlApi = `${environment.urlWebApi}api/Account`;
    return this.http.post<PersonFull>(urlApi, createAccount);
  }

  consultingBalance(accountNumber: string) {
    const urlApi = `${environment.urlWebApi}api/Account/${accountNumber}`;
    return this.http.get<AcountFull>(urlApi);
  }

  makeDeposit(transaction: any) {
    const urlApi = `${environment.urlWebApi}api/Account/MakeDeposit`;
    return this.http.put<any>(urlApi, transaction);
  }

  withdrawal(transaction: any) {
    const urlApi = `${environment.urlWebApi}api/Account/MakeWithdrawal`;
    return this.http.put<any>(urlApi, transaction);
  }

  getParams(obj): HttpParams {
    return Object.keys(obj).reduce((params, key) =>
      obj[key] ? params.append(key, obj[key]) : params, new HttpParams());
  }
}
