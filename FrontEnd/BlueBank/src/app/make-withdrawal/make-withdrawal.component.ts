import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AcountFull} from '../models/acount-full';
import {CreateAccountService} from '../services/create-account.service';
import Swal from 'sweetalert2';
import {jsPDF} from 'jspdf';
import {formatCurrency} from '@angular/common';

@Component({
  selector: 'app-make-withdrawal',
  templateUrl: './make-withdrawal.component.html',
  styleUrls: ['./make-withdrawal.component.css']
})
export class MakeWithdrawalComponent implements OnInit {
  form: FormGroup;
  account: AcountFull = new AcountFull(0, '', 0);
  balance: number;
  constructor(private accountServie: CreateAccountService) {
    this.form = this.createForm();
  }

  ngOnInit() {
  }

  submit() {
    if (this.form.valid) {
      this.transaction();
    } else {
      Swal.fire({
        title: 'Campos Obligatorios',
        text: 'Diligencie los campos porfavor',
        icon: 'warning',
        confirmButtonText: 'Cerrar'
      });
    }
  }
  transaction() {
    const numberAccount = this.numberAccount.value.trim();
    this.accountServie.consultingBalance(numberAccount).subscribe(response => {
      this.account = {id: response.id, numberAccount: response.numberAccount, balance: response.balance };
      this.accountServie.withdrawal({numberAccount: numberAccount, value: this.deposit.value}).subscribe(responseTransaction => {
        this.balance = this.account.balance - this.deposit.value;
        Swal.fire({
          title: 'Exito',
          text: 'Retiro de dinero',
          icon: 'success',
          timer: 3000,
          confirmButtonText: 'Cerrar'
        });
      });

    }, error => {
      this.form.reset();
      this.account = new AcountFull(0, '', 0);
      this.balance = null;
      Swal.fire({
        title: 'No se encontraron resultado',
        text: 'No se encuentra un resultado con el numero de cuenta ' + numberAccount,
        icon: 'info',
        confirmButtonText: 'Cerrar'
      });
    });
  }


  get numberAccount() { return this.form.get('NumberAccount'); }
  get deposit() { return this.form.get('Deposit'); }
  createForm() {
    return new FormGroup({
      NumberAccount: new FormControl('', Validators.required),
      Deposit: new FormControl('', Validators.required)
    });
  }

  GeneratePdf() {
    const date = new Date();
    const doc = new jsPDF();
    doc.setFontSize(20);
    doc.text('Saldos con Blue Bank' , 60, 10);
    doc.setFontSize(12);
    doc.text('Fecha de Consulta: ' + date.toString() , 10, 20);
    doc.text('Su número de cuenta es: ' + this.numberAccount.value, 10, 30);
    doc.text('Su saldo de cuenta es: $' + formatCurrency(this.balance, 'en', ''), 10, 40);
    doc.save( `${this.numberAccount.value}-BlueBank.pdf`);
  }
}
