import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeWithdrawalComponent } from './make-withdrawal.component';

describe('MakeWithdrawalComponent', () => {
  let component: MakeWithdrawalComponent;
  let fixture: ComponentFixture<MakeWithdrawalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakeWithdrawalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
